export default function CreateFileName(opts) {
    let suffix = opts.type.toLowerCase()
    return opts.fileName + '.' + suffix
}