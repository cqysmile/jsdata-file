export default function FormatData(opts) {
  //如果是文件流则直接返回
  if (opts.data instanceof Blob) {
    return opts.data
  }

  let data = "";

  let suffix = opts.type.toLowerCase();

  data = JSON.stringify(opts.data);
  
  if (suffix === "js") {
      data = "let result = " + data
  }

  return data
}
