let path = require("path");

module.exports = {
  mode: "production",
  // entry: path.resolve(__dirname, "main.js"),
  // output: {
  //   path: path.resolve(__dirname, "dist"),
  //   filename: "main.js",
  // },
  entry: {
    main: path.join(__dirname, "./index.js"),
  },
  context: path.join(__dirname, "./"),
  output: {
    path: path.join(__dirname, "dist"),
    filename: "js-data-file.js",
    // publicPath: "/",
  },

  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [["@babel/preset-env", { targets: "defaults" }]],
          },
        },
      },
    ],
  },

  devServer: {
    contentBase: path.join(__dirname, ""),
    compress: true,
    port: 9000,
    open: true,
    openPage: "test/",
  },

  // optimization: {
  //   usedExports: true,
  // },
};
