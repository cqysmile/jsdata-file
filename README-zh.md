# jsdata-file

> 提供文件流下载，以及js数据导出成文件的功能

## 使用

```bash

# 安装依赖
npm install jsdata-file

# 引入
import jsdataFile from jsdata-file

# 调用
jsdataFile(opts)
```
## 属性

| 参数 | 说明 | 默认值 |
| ------- | ------- | ------- |
|    type     |   生成文件类型，填入文件后缀     |      js   |
|    data     |   文件类容(String、Number、Object、Blob、Array等数据类型数据)     |      null   |
|    fileName     |   文件名     |      测试   |