/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./index.js":
/*!******************!*\
  !*** ./index.js ***!
  \******************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("/* unused harmony export default */\n/* harmony import */ var _utils_merge_opts_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utils/merge-opts.js */ \"./utils/merge-opts.js\");\n/* harmony import */ var _utils_create_fileName_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utils/create-fileName.js */ \"./utils/create-fileName.js\");\n/* harmony import */ var _utils_get_contentType_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils/get-contentType.js */ \"./utils/get-contentType.js\");\n/* harmony import */ var _utils_download_file_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./utils/download-file.js */ \"./utils/download-file.js\");\n\n\n\n\nfunction main(opts) {\n  opts = (0,_utils_merge_opts_js__WEBPACK_IMPORTED_MODULE_0__.default)(opts, defaultCfg);\n  var fileName = (0,_utils_create_fileName_js__WEBPACK_IMPORTED_MODULE_1__.default)(opts);\n  var contentType = (0,_utils_get_contentType_js__WEBPACK_IMPORTED_MODULE_2__.default)(opts);\n  (0,_utils_download_file_js__WEBPACK_IMPORTED_MODULE_3__.default)(opts.data, contentType, fileName);\n}\nvar defaultCfg = {\n  type: 'JS',\n  data: null,\n  fileName: '测试'\n};\n\n//# sourceURL=webpack://download-js-data/./index.js?");

/***/ }),

/***/ "./utils/create-fileName.js":
/*!**********************************!*\
  !*** ./utils/create-fileName.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ CreateFileName)\n/* harmony export */ });\nfunction CreateFileName(opts) {\n  var suffix = opts.type.toLowerCase();\n  return opts.fileName + '.' + suffix;\n}\n\n//# sourceURL=webpack://download-js-data/./utils/create-fileName.js?");

/***/ }),

/***/ "./utils/download-file.js":
/*!********************************!*\
  !*** ./utils/download-file.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ downloadFile)\n/* harmony export */ });\nfunction downloadFile(data, type, fileName) {\n  var blob = new Blob([data], {\n    type: \"\".concat(type, \";charset=utf-8\")\n  });\n  var downloadElement = document.createElement('a'); // 创建下载的链接\n\n  var href = window.URL.createObjectURL(blob);\n  downloadElement.href = href; // 下载后文件名\n\n  downloadElement.download = fileName || 'arguments.txt';\n  document.body.appendChild(downloadElement); // 点击下载\n\n  downloadElement.click(); // 下载完成移除元素\n\n  document.body.removeChild(downloadElement); // 释放掉blob对象\n\n  window.URL.revokeObjectURL(href);\n}\n\n//# sourceURL=webpack://download-js-data/./utils/download-file.js?");

/***/ }),

/***/ "./utils/get-contentType.js":
/*!**********************************!*\
  !*** ./utils/get-contentType.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ GetContentType)\n/* harmony export */ });\nfunction GetContentType(opts) {\n  var type = opts.type.toLowerCase();\n  var contentType = contentTypeMap[type];\n  return contentType;\n}\nvar contentTypeMap = {\n  js: \"application/javascript\",\n  json: \"application/json\",\n  txt: \"text/plain\"\n};\n\n//# sourceURL=webpack://download-js-data/./utils/get-contentType.js?");

/***/ }),

/***/ "./utils/merge-opts.js":
/*!*****************************!*\
  !*** ./utils/merge-opts.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ MergeOpts)\n/* harmony export */ });\nfunction MergeOpts(opts, defaulltCfg) {\n  return Object.assign({}, defaulltCfg, opts || {});\n}\n\n//# sourceURL=webpack://download-js-data/./utils/merge-opts.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./index.js");
/******/ 	
/******/ })()
;