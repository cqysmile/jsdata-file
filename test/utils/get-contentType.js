export default function GetContentType(opts) {
    let type = opts.type.toLowerCase()
    let contentType = contentTypeMap[type]

    return contentType
}

var contentTypeMap = {
    js: "application/javascript",
    json: "application/json",
    txt: "text/plain"
}