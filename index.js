import MergeOpts from "./utils/merge-opts.js";
import CreateFileName from "./utils/create-fileName.js";
import GetContentType from "./utils/get-contentType.js";
import DownloadFile from "./utils/download-file.js";
import FormatData from "./utils/formate-data.js";

function main(opts) {
  opts = MergeOpts(opts, defaultCfg);

  let fileName = CreateFileName(opts);

  let contentType = GetContentType(opts);

  let data = FormatData(opts)

  DownloadFile(data, contentType, fileName);
}

export default main

var defaultCfg = {
  type: "JS",
  data: null,
  fileName: "测试",
};
